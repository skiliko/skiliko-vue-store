## v1.5.0 (December 16, 2019)

Connect real time with purchase
Fix issue with expert/messages
Update sdk dependency

## v1.4.1 (September 22, 2019)

Fix issue with conversation's id

## v1.4.0 (September 01, 2019)

Add platform administrator in the expert list

## v1.3.0 (July 01, 2018)

Add thread offers

## v1.2.0 (June 03, 2018)

Add product offers

## v1.1.0 (March 11, 2018)

Add public offers function on offer store

## v1.0.0 (Aug 17, 2017)

Creation of the lib
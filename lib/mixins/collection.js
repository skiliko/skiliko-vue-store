'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _collectionKey = require('../utils/collectionKey');

var _collectionKey2 = _interopRequireDefault(_collectionKey);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

exports.default = function (collectionName) {
  var _computed;

  var payload = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : function (x) {
    return {};
  };
  return {
    computed: (_computed = {}, _defineProperty(_computed, 'collections[' + collectionName + ']', function undefined() {
      return this.$store.getters['skiliko/' + collectionName + '/collection'];
    }), _defineProperty(_computed, 'pages[' + collectionName + ']', function undefined() {
      return this.$store.getters['skiliko/' + collectionName + '/pages'];
    }), _defineProperty(_computed, 'paginations[' + collectionName + ']', function undefined() {
      return this.$store.getters['skiliko/' + collectionName + '/paginations'];
    }), _defineProperty(_computed, collectionName + 'Pagination', function undefined() {
      var key = (0, _collectionKey2.default)(payload(this) || {});
      return this['paginations[' + collectionName + ']'][key] || {};
    }), _defineProperty(_computed, collectionName, function () {
      var _this = this;

      var key = (0, _collectionKey2.default)(payload(this) || {});
      return (this['pages[' + collectionName + ']'][key] || []).map(function (id) {
        return _this['collections[' + collectionName + ']'][id];
      });
    }), _computed)
  };
};
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = {
  computed: {
    isConnected: function isConnected() {
      return this.$store.getters['skiliko/session/isConnected'];
    },
    currentUser: function currentUser() {
      return this.$store.getters['skiliko/user/current'];
    }
  },
  watch: {
    isConnected: function isConnected() {
      this.handleConnect();
    }
  },
  methods: {
    handleConnect: function handleConnect() {
      if (this.isConnected && this.onLogin) {
        this.onLogin();
      }
      if (!this.isConnected && this.onLogout) {
        this.onLogout();
      }
    }
  },
  mounted: function mounted() {
    this.handleConnect();
  }
};
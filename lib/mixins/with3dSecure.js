'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (callback) {
  return {
    data: function data() {
      return {
        frame3dSecureLoaded: false
      };
    },

    methods: {
      on3DSecureFrameLoaded: function on3DSecureFrameLoaded(frame) {
        this.frame3dSecureLoaded = true;
        window.addEventListener('message', this.handle3DSecureMessage);
      },
      handle3DSecureMessage: function handle3DSecureMessage(message) {
        if (!message || !message.data) {
          return;
        }
        if (message.data.type !== '3dsecure') {
          return;
        }
        this.frame3dSecureLoaded = false;
        if (!this[callback]) {
          throw new Error('You need to implement on3dSecureValidated method with with3dSecure mixin');
        }
        this[callback]();
      }
    }
  };
};
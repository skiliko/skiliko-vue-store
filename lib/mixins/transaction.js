'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function () {
  var item = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'transaction';
  return {
    computed: {
      transactionType: function transactionType() {
        return this[item].offer.service_type;
      },
      isService: function isService() {
        return this.transactionType === 'service';
      },
      isMail: function isMail() {
        return this.transactionType === 'mail';
      },
      isProduct: function isProduct() {
        return this.transactionType === 'product';
      },
      isThread: function isThread() {
        return this.transactionType === 'thread';
      },
      isCall: function isCall() {
        return this.transactionType === 'phone';
      },
      isPrepaidMinutes: function isPrepaidMinutes() {
        return this.transactionType === 'prepaid_minute';
      },
      isPayed: function isPayed() {
        return !!this[item].payed_at;
      },
      isAccepted: function isAccepted() {
        return !!this[item].accepted_at;
      },
      isFinished: function isFinished() {
        return !!this[item].finished_at;
      },
      isRefunded: function isRefunded() {
        return !!this[item].refunded_at;
      },
      needPayment: function needPayment() {
        return this[item].offer.price !== 0;
      },
      status: function status() {
        if (this.isRefunded) {
          return 'refunded';
        }
        if (this.isFinished) {
          return 'finished';
        }
        if (this.isAccepted) {
          return 'inProgress';
        }
        if (this.isPayed) {
          return 'pending';
        }
        throw new Error('Status is not valid for #' + this[item].id);
      }
    }
  };
};
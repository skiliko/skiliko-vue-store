'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = {
  computed: {
    platform: function platform() {
      return this.$store.getters['skiliko/platform/current'];
    }
  }
};
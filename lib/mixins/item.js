'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _pluralize = require('../utils/pluralize');

var _pluralize2 = _interopRequireDefault(_pluralize);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

exports.default = function (resource) {
  var _computed;

  var idAttribute = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'id';
  return {
    computed: (_computed = {}, _defineProperty(_computed, resource + 'Collection', function undefined() {
      return this.$store.getters['skiliko/' + (0, _pluralize2.default)(resource) + '/collection'];
    }), _defineProperty(_computed, resource, function () {
      var id = this[idAttribute] || this.$route.params[idAttribute];
      return this[resource + 'Collection'][id];
    }), _computed),
    mounted: function mounted() {
      var idValue = this[idAttribute] || this.$route.params[idAttribute];
      if (!idValue) {
        throw new Error('Cannot find an ID for the ' + resource + ' with the attribute ' + idAttribute);
      }
    }
  };
};
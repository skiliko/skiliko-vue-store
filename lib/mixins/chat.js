'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (withAttributeName) {
  return {
    computed: {
      conversations: function conversations() {
        return this.$store.getters['skiliko/conversations/collection'];
      },
      conversationId: function conversationId() {
        var _this = this;

        var conversations = Object.keys(this.conversations).filter(function (id) {
          return _this.conversations[id].with._id === _this[withAttributeName];
        });
        if (conversations.length === 1) {
          return conversations[0];
        }
        return conversations.filter(function (id) {
          return id && id !== 'undefined';
        })[0];
      },
      conversation: function conversation() {
        return this.conversations[this.conversationId];
      },
      allMessages: function allMessages() {
        return this.$store.getters['skiliko/messages/collection'];
      },
      messages: function messages() {
        var _this2 = this;

        return Object.keys(this.allMessages).filter(function (x) {
          return typeof _this2.allMessages[x].conversation === 'string' ? _this2.allMessages[x].conversation === _this2.conversationId : _this2.allMessages[x].conversation._id === _this2.conversationId;
        }).map(function (x) {
          return _this2.allMessages[x];
        }).sort(function (a, b) {
          return a.sentAt.localeCompare(b.sentAt);
        });
      }
    },
    methods: {
      sendMessage: function sendMessage(_ref) {
        var message = _ref.message,
            _ref$data = _ref.data,
            data = _ref$data === undefined ? {} : _ref$data;

        return this.$store.dispatch('skiliko/messages/create', {
          message: message,
          data: data,
          to: this.conversation.with._id
        });
      },
      markAsRead: function markAsRead(_ref2) {
        var _this3 = this;

        var messages = _ref2.messages;

        var ids = this.messages.filter(function (message) {
          return !message.readAt;
        }).filter(function (message) {
          return message.from._id === _this3.conversation.with._id;
        }).map(function (m) {
          return m._id;
        });
        return this.$store.dispatch('skiliko/messages/markAllAsRead', {
          ids: ids
        });
      }
    }
  };
};
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.mixins = undefined;

var _modules = require('./modules');

var _modules2 = _interopRequireDefault(_modules);

var _mixins = require('./mixins');

var _mixins2 = _interopRequireDefault(_mixins);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (store) {
  return store.registerModule('skiliko', {
    namespaced: true,
    modules: _modules2.default
  });
};

exports.mixins = _mixins2.default;
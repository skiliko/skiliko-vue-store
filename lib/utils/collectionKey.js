'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var transform = function transform(data) {
  return Object.keys(data).sort().filter(function (e) {
    return data[e];
  }).reduce(function (acc, item) {
    return Object.assign({}, acc, _defineProperty({}, item, data[item]));
  }, {});
};

exports.default = function (data) {
  return Object.keys(transform(data)).map(function (item) {
    return [item, data[item]].join('=');
  }).join('&') || 'default';
};
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _formatHourMinuteMonth = require('./formatHourMinuteMonth');

var _formatHourMinuteMonth2 = _interopRequireDefault(_formatHourMinuteMonth);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var dateIso = function dateIso(date) {
  return [date.getUTCFullYear(), (0, _formatHourMinuteMonth2.default)(date.getUTCMonth() + 1), (0, _formatHourMinuteMonth2.default)(date.getUTCDate())].join('-');
};

var timeIso = function timeIso(date) {
  return [(0, _formatHourMinuteMonth2.default)(date.getUTCHours()), (0, _formatHourMinuteMonth2.default)(date.getUTCMinutes()), '00'].join(':');
};

var iso = function iso(date) {
  return {
    date: dateIso(date),
    hour: timeIso(date)
  };
};

exports.default = function (date) {
  return iso(date).date + 'T' + iso(date).hour + '.000Z';
};
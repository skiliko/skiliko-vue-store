"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = {
  state: {
    isLoading: false
  },
  mutations: {
    changeLoading: function changeLoading(state, _ref) {
      var isLoading = _ref.isLoading;
      return state.isLoading = isLoading;
    }
  },
  getters: {
    isLoading: function isLoading(state) {
      return state.isLoading;
    }
  }
};
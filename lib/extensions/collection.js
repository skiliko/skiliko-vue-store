'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _collectionKey = require('../utils/collectionKey');

var _collectionKey2 = _interopRequireDefault(_collectionKey);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var _updateItem = function _updateItem(state, item) {
  return _replaceItem(state, Object.assign({}, state.collectionById[item.id || item._id], item));
};

var _replaceItem = function _replaceItem(state, item) {
  return state.collectionById = Object.assign({}, state.collectionById, _defineProperty({}, item.id || item._id, item));
};

var _updateCollection = function _updateCollection(state, collection) {
  var payload = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

  state.collectionById = Object.assign({}, state.collectionById, collection.reduce(function (acc, item) {
    return Object.assign({}, acc, _defineProperty({}, item.id || item._id, item));
  }, {}));
  state.pages = Object.assign({}, state.pages, _defineProperty({}, (0, _collectionKey2.default)(payload), collection.map(function (x) {
    return x.id || x._id;
  })));

  state.paginations = Object.assign({}, state.paginations, _defineProperty({}, (0, _collectionKey2.default)(payload), collection.pagination));
};

var ACTIONS = {
  fetchAll: function fetchAll(executor) {
    return function () {
      var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(_ref, payload) {
        var commit = _ref.commit;
        var collection;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return executor(payload);

              case 2:
                collection = _context.sent;

                commit('updateCollection', { collection: collection, payload: payload });
                return _context.abrupt('return', collection);

              case 5:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, undefined);
      }));

      return function (_x2, _x3) {
        return _ref2.apply(this, arguments);
      };
    }();
  },
  get: function get(executor) {
    return function () {
      var _ref5 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(_ref3, _ref4) {
        var commit = _ref3.commit;
        var id = _ref4.id;
        var item;
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return executor(id);

              case 2:
                item = _context2.sent;

                commit('updateItem', { item: item });
                return _context2.abrupt('return', item);

              case 5:
              case 'end':
                return _context2.stop();
            }
          }
        }, _callee2, undefined);
      }));

      return function (_x4, _x5) {
        return _ref5.apply(this, arguments);
      };
    }();
  }
};

var generateActions = function generateActions() {
  var actions = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

  if (Object.keys(actions).some(function (x) {
    return Object.keys(ACTIONS).indexOf(x) < 0;
  })) {
    throw new Error('Only actions ' + Object.keys(ACTIONS).join(', ') + ' are available !');
  }
  return Object.keys(actions).reduce(function (acc, action) {
    return Object.assign({}, acc, _defineProperty({}, action, ACTIONS[action](actions[action])));
  }, {});
};

exports.default = function () {
  var actions = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  return {
    state: {
      collectionById: {},
      pages: {},
      paginations: {}
    },
    getters: {
      collection: function collection(state) {
        return state.collectionById;
      },
      pages: function pages(state) {
        return state.pages;
      },
      paginations: function paginations(state) {
        return state.paginations;
      }
    },
    mutations: {
      updateItem: function updateItem(state, _ref6) {
        var item = _ref6.item;
        return _updateItem(state, item);
      },
      replaceItem: function replaceItem(state, _ref7) {
        var item = _ref7.item;
        return _replaceItem(state, item);
      },
      updateCollection: function updateCollection(state, _ref8) {
        var collection = _ref8.collection,
            payload = _ref8.payload;
        return _updateCollection(state, collection, payload);
      }
    },
    actions: generateActions(actions)
  };
};
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _skilikoSdk = require('skiliko-sdk');

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.default = {
  fetchAll: function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(_ref, payload) {
      var commit = _ref.commit,
          dispatch = _ref.dispatch,
          rootGetters = _ref.rootGetters;
      var platform, collection;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              platform = rootGetters['skiliko/platform/current'];

              if (platform.multi_experts) {
                _context.next = 3;
                break;
              }

              return _context.abrupt('return');

            case 3:
              _context.next = 5;
              return _skilikoSdk.Expert.all();

            case 5:
              collection = _context.sent;

              commit('updateCollection', { collection: collection, payload: payload });

            case 7:
            case 'end':
              return _context.stop();
          }
        }
      }, _callee, undefined);
    }));

    function fetchAll(_x, _x2) {
      return _ref2.apply(this, arguments);
    }

    return fetchAll;
  }()
};
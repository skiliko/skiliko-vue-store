'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.default = {
  fetchAll: function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(_ref) {
      var commit = _ref.commit,
          dispatch = _ref.dispatch;

      var _ref3 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : { page: 1 },
          _ref3$page = _ref3.page,
          page = _ref3$page === undefined ? 1 : _ref3$page;

      var chat, collection;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return dispatch('skiliko/chat/get', {}, { root: true });

            case 2:
              chat = _context.sent;
              _context.next = 5;
              return chat.conversations(page);

            case 5:
              collection = _context.sent;

              commit('updateCollection', { collection: collection, payload: { page: page } });
              return _context.abrupt('return', collection);

            case 8:
            case 'end':
              return _context.stop();
          }
        }
      }, _callee, undefined);
    }));

    function fetchAll(_x2) {
      return _ref2.apply(this, arguments);
    }

    return fetchAll;
  }(),
  find: function () {
    var _ref6 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(_ref4, _ref5) {
      var commit = _ref4.commit,
          dispatch = _ref4.dispatch;
      var id = _ref5.id;
      var chat, item;
      return regeneratorRuntime.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _context2.next = 2;
              return dispatch('skiliko/chat/get', {}, { root: true });

            case 2:
              chat = _context2.sent;
              _context2.next = 5;
              return chat.conversationWith(id);

            case 5:
              item = _context2.sent;

              commit('updateItem', { item: item });
              return _context2.abrupt('return', item);

            case 8:
            case 'end':
              return _context2.stop();
          }
        }
      }, _callee2, undefined);
    }));

    function find(_x3, _x4) {
      return _ref6.apply(this, arguments);
    }

    return find;
  }()
};
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _merge = require('../../utils/merge');

var _merge2 = _interopRequireDefault(_merge);

var _collection = require('../../extensions/collection');

var _collection2 = _interopRequireDefault(_collection);

var _actions = require('./actions');

var _actions2 = _interopRequireDefault(_actions);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

exports.default = (0, _merge2.default)({}, (0, _collection2.default)(), {
  namespaced: true,
  getters: {
    pending: function pending(state, getters, rootState, rootGetters) {
      return rootGetters['skiliko/messages/pending'].reduce(function (conversations, message) {
        return Object.assign({}, conversations, _defineProperty({}, message.conversation, Array.concat(conversations[message.conversation] || [], [message])));
      }, {});
    }
  },
  actions: _actions2.default
});
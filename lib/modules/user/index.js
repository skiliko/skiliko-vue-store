'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _actions = require('./actions');

var _actions2 = _interopRequireDefault(_actions);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  namespaced: true,
  state: {
    current: null
  },
  getters: {
    current: function current(state) {
      return state.current;
    }
  },
  mutations: {
    setCurrent: function setCurrent(state, _ref) {
      var user = _ref.user;
      return state.current = user;
    }
  },
  actions: _actions2.default
};
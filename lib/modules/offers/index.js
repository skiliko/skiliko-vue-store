'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _skilikoSdk = require('skiliko-sdk');

var _merge = require('../../utils/merge');

var _merge2 = _interopRequireDefault(_merge);

var _collection = require('../../extensions/collection');

var _collection2 = _interopRequireDefault(_collection);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = (0, _merge2.default)({}, (0, _collection2.default)({
  get: _skilikoSdk.Offer.get,
  fetchAll: _skilikoSdk.Offer.publicOffers
}), {
  namespaced: true
});
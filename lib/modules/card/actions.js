'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _skilikoSdk = require('skiliko-sdk');

var _formatHourMinuteMonth = require('../../utils/formatHourMinuteMonth');

var _formatHourMinuteMonth2 = _interopRequireDefault(_formatHourMinuteMonth);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.default = {
  create: function () {
    var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(_ref, _ref2) {
      var dispatch = _ref.dispatch;
      var number = _ref2.number,
          month = _ref2.month,
          year = _ref2.year,
          cvx = _ref2.cvx,
          firstName = _ref2.firstName,
          lastName = _ref2.lastName,
          birthday = _ref2.birthday;
      var card;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return dispatch('skiliko/user/save', { first_name: firstName, last_name: lastName, birthday: birthday }, { root: true });

            case 2:
              _context.next = 4;
              return _skilikoSdk.Card.create(number, (0, _formatHourMinuteMonth2.default)(month), year, cvx);

            case 4:
              card = _context.sent;
              _context.next = 7;
              return dispatch('skiliko/user/save', { card_id: card.id }, { root: true });

            case 7:
              return _context.abrupt('return', card);

            case 8:
            case 'end':
              return _context.stop();
          }
        }
      }, _callee, undefined);
    }));

    function create(_x, _x2) {
      return _ref3.apply(this, arguments);
    }

    return create;
  }()
};
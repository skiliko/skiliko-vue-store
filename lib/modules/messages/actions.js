'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _skilikoSdk = require('skiliko-sdk');

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.default = {
  fetchAll: function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(_ref) {
      var commit = _ref.commit,
          dispatch = _ref.dispatch;

      var _ref3 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : { page: 1 },
          conversationId = _ref3.conversationId,
          _ref3$page = _ref3.page,
          page = _ref3$page === undefined ? 1 : _ref3$page;

      var chat, collection;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return dispatch('skiliko/chat/get', {}, { root: true });

            case 2:
              chat = _context.sent;
              _context.next = 5;
              return chat.messages(conversationId, page);

            case 5:
              collection = _context.sent;

              commit('updateCollection', { collection: collection, payload: { conversationId: conversationId, page: page } });
              return _context.abrupt('return', collection);

            case 8:
            case 'end':
              return _context.stop();
          }
        }
      }, _callee, undefined);
    }));

    function fetchAll(_x2) {
      return _ref2.apply(this, arguments);
    }

    return fetchAll;
  }(),
  fetchPending: function () {
    var _ref5 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(_ref4) {
      var commit = _ref4.commit,
          dispatch = _ref4.dispatch;
      var chat, collection;
      return regeneratorRuntime.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _context2.next = 2;
              return dispatch('skiliko/chat/get', {}, { root: true });

            case 2:
              chat = _context2.sent;
              _context2.next = 5;
              return _skilikoSdk.Chat.pendingMessages(chat.from);

            case 5:
              collection = _context2.sent;

              commit('updateCollection', { collection: collection, payload: { scope: 'pendings' } });
              return _context2.abrupt('return', collection);

            case 8:
            case 'end':
              return _context2.stop();
          }
        }
      }, _callee2, undefined);
    }));

    function fetchPending(_x3) {
      return _ref5.apply(this, arguments);
    }

    return fetchPending;
  }(),
  create: function () {
    var _ref8 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(_ref6, _ref7) {
      var commit = _ref6.commit,
          dispatch = _ref6.dispatch;
      var message = _ref7.message,
          to = _ref7.to,
          data = _ref7.data;
      var chat, item;
      return regeneratorRuntime.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              _context3.next = 2;
              return dispatch('skiliko/chat/get', {}, { root: true });

            case 2:
              chat = _context3.sent;
              _context3.next = 5;
              return chat.send(message, to, data);

            case 5:
              item = _context3.sent;

              commit('replaceItem', { item: item });
              return _context3.abrupt('return', item);

            case 8:
            case 'end':
              return _context3.stop();
          }
        }
      }, _callee3, undefined);
    }));

    function create(_x4, _x5) {
      return _ref8.apply(this, arguments);
    }

    return create;
  }(),
  markAllAsRead: function () {
    var _ref11 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(_ref9, _ref10) {
      var commit = _ref9.commit,
          dispatch = _ref9.dispatch;
      var ids = _ref10.ids;
      var chat;
      return regeneratorRuntime.wrap(function _callee4$(_context4) {
        while (1) {
          switch (_context4.prev = _context4.next) {
            case 0:
              _context4.next = 2;
              return dispatch('skiliko/chat/get', {}, { root: true });

            case 2:
              chat = _context4.sent;
              _context4.next = 5;
              return chat.markAsRead(ids);

            case 5:
            case 'end':
              return _context4.stop();
          }
        }
      }, _callee4, undefined);
    }));

    function markAllAsRead(_x6, _x7) {
      return _ref11.apply(this, arguments);
    }

    return markAllAsRead;
  }(),
  markAsReceived: function () {
    var _ref14 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(_ref12, _ref13) {
      var commit = _ref12.commit,
          dispatch = _ref12.dispatch,
          getters = _ref12.getters;
      var id = _ref13.id;
      var chat, message;
      return regeneratorRuntime.wrap(function _callee5$(_context5) {
        while (1) {
          switch (_context5.prev = _context5.next) {
            case 0:
              _context5.next = 2;
              return dispatch('skiliko/chat/get', {}, { root: true });

            case 2:
              chat = _context5.sent;
              message = getters.collection[id];

              if (id) {
                _context5.next = 6;
                break;
              }

              return _context5.abrupt('return');

            case 6:
              if (!(message.from._id === chat.from)) {
                _context5.next = 8;
                break;
              }

              return _context5.abrupt('return');

            case 8:
              if (!message.receivedAt) {
                _context5.next = 10;
                break;
              }

              return _context5.abrupt('return');

            case 10:
              _context5.next = 12;
              return chat.markAsRead(id);

            case 12:
            case 'end':
              return _context5.stop();
          }
        }
      }, _callee5, undefined);
    }));

    function markAsReceived(_x8, _x9) {
      return _ref14.apply(this, arguments);
    }

    return markAsReceived;
  }()
};
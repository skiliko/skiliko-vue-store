'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _merge = require('../../utils/merge');

var _merge2 = _interopRequireDefault(_merge);

var _collection = require('../../extensions/collection');

var _collection2 = _interopRequireDefault(_collection);

var _actions = require('./actions');

var _actions2 = _interopRequireDefault(_actions);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = (0, _merge2.default)({}, (0, _collection2.default)(), {
  namespaced: true,
  getters: {
    pending: function pending(state, getters, rootState, rootGetters) {
      return rootGetters['skiliko/user/current'] ? Object.keys(getters.collection).map(function (id) {
        return getters.collection[id];
      }).filter(function (message) {
        return !!message.to;
      }).filter(function (message) {
        return message.to.scope === 'user';
      }).filter(function (message) {
        return message.to._id === rootGetters['skiliko/user/current'].chatable_id;
      }).filter(function (message) {
        return !message.readAt;
      }) : [];
    }
  },
  actions: _actions2.default
});
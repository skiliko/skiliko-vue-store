'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _skilikoSdk = require('skiliko-sdk');

var _merge = require('../../utils/merge');

var _merge2 = _interopRequireDefault(_merge);

var _collection = require('../../extensions/collection');

var _collection2 = _interopRequireDefault(_collection);

var _actions = require('./actions');

var _actions2 = _interopRequireDefault(_actions);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = (0, _merge2.default)({}, (0, _collection2.default)({
  fetchAll: function fetchAll(payload) {
    return _skilikoSdk.Rating.all(payload.page, payload.expertId);
  }
}), {
  namespaced: true,
  actions: _actions2.default
});
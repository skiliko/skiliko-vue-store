'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _skilikoSdk = require('skiliko-sdk');

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.default = {
  create: function () {
    var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(_ref, _ref2) {
      var commit = _ref.commit,
          dispatch = _ref.dispatch;
      var purchaseId = _ref2.purchaseId,
          purchase = _ref2.purchase,
          note = _ref2.note,
          content = _ref2.content;
      var item;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return _skilikoSdk.Rating.create(purchaseId || purchase.id, note, content);

            case 2:
              item = _context.sent;

              commit('replaceItem', { item: item });
              return _context.abrupt('return', item);

            case 5:
            case 'end':
              return _context.stop();
          }
        }
      }, _callee, undefined);
    }));

    function create(_x, _x2) {
      return _ref3.apply(this, arguments);
    }

    return create;
  }()
};
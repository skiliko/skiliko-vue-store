'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _skilikoSdk = require('skiliko-sdk');

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.default = {
  sendInstructions: function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(_, _ref) {
      var email = _ref.email;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return _skilikoSdk.Password.sendInstructions(email);

            case 2:
            case 'end':
              return _context.stop();
          }
        }
      }, _callee, undefined);
    }));

    function sendInstructions(_x, _x2) {
      return _ref2.apply(this, arguments);
    }

    return sendInstructions;
  }(),
  reset: function () {
    var _ref4 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(_, _ref3) {
      var token = _ref3.token,
          password = _ref3.password,
          passwordConfirmation = _ref3.passwordConfirmation;
      return regeneratorRuntime.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _context2.next = 2;
              return _skilikoSdk.Password.reset(token, password, passwordConfirmation);

            case 2:
            case 'end':
              return _context2.stop();
          }
        }
      }, _callee2, undefined);
    }));

    function reset(_x3, _x4) {
      return _ref4.apply(this, arguments);
    }

    return reset;
  }()
};
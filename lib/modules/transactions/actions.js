'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _skilikoSdk = require('skiliko-sdk');

var _dateToIso = require('../../utils/dateToIso');

var _dateToIso2 = _interopRequireDefault(_dateToIso);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var processData = function processData() {
  var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  return Object.assign({}, data, {
    attachments: data.attachments ? JSON.stringify(data.attachments) : null,
    call_at: data.call_at ? (0, _dateToIso2.default)(new Date(data.call_at)) : null
  });
};

exports.default = {
  create: function () {
    var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(_ref, _ref2) {
      var commit = _ref.commit,
          dispatch = _ref.dispatch;
      var offer = _ref2.offer,
          offerId = _ref2.offerId,
          expert = _ref2.expert,
          expertId = _ref2.expertId;
      var platform, item;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return dispatch('skiliko/platform/fetch', {}, { root: true });

            case 2:
              platform = _context.sent;
              _context.next = 5;
              return _skilikoSdk.Purchase.create(offer || { id: offerId }, platform.multi_experts ? expert || { id: expertId } : null);

            case 5:
              item = _context.sent;

              commit('updateItem', { item: item });
              return _context.abrupt('return', item);

            case 8:
            case 'end':
              return _context.stop();
          }
        }
      }, _callee, undefined);
    }));

    function create(_x2, _x3) {
      return _ref3.apply(this, arguments);
    }

    return create;
  }(),
  update: function () {
    var _ref6 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(_ref4, _ref5) {
      var commit = _ref4.commit;
      var purchase = _ref5.purchase,
          transaction = _ref5.transaction,
          data = _ref5.data;
      var item;
      return regeneratorRuntime.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _context2.next = 2;
              return _skilikoSdk.Purchase.save(purchase || transaction, processData(data));

            case 2:
              item = _context2.sent;

              commit('updateItem', { item: item });
              return _context2.abrupt('return', item);

            case 5:
            case 'end':
              return _context2.stop();
          }
        }
      }, _callee2, undefined);
    }));

    function update(_x4, _x5) {
      return _ref6.apply(this, arguments);
    }

    return update;
  }(),
  pay: function () {
    var _ref9 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(_ref7, _ref8) {
      var commit = _ref7.commit,
          dispatch = _ref7.dispatch;
      var purchase = _ref8.purchase,
          transaction = _ref8.transaction,
          cardId = _ref8.cardId,
          card = _ref8.card;
      var cardInformations, item;
      return regeneratorRuntime.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              if (!(cardId || !card)) {
                _context3.next = 6;
                break;
              }

              _context3.next = 3;
              return Promise.resolve({ id: cardId });

            case 3:
              _context3.t0 = _context3.sent;
              _context3.next = 9;
              break;

            case 6:
              _context3.next = 8;
              return dispatch('skiliko/card/create', card, { root: true });

            case 8:
              _context3.t0 = _context3.sent;

            case 9:
              cardInformations = _context3.t0;
              _context3.next = 12;
              return _skilikoSdk.Purchase.pay(Object.assign({}, purchase || transaction, { card_id: cardInformations.id }));

            case 12:
              item = _context3.sent;

              commit('updateItem', { item: item });
              return _context3.abrupt('return', item);

            case 15:
            case 'end':
              return _context3.stop();
          }
        }
      }, _callee3, undefined);
    }));

    function pay(_x6, _x7) {
      return _ref9.apply(this, arguments);
    }

    return pay;
  }(),
  refund: function () {
    var _ref12 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(_ref10, _ref11) {
      var commit = _ref10.commit;
      var purchase = _ref11.purchase,
          transaction = _ref11.transaction;
      var item;
      return regeneratorRuntime.wrap(function _callee4$(_context4) {
        while (1) {
          switch (_context4.prev = _context4.next) {
            case 0:
              _context4.next = 2;
              return _skilikoSdk.Purchase.refund(purchase || transaction);

            case 2:
              item = _context4.sent;

              commit('updateItem', { item: item });
              return _context4.abrupt('return', item);

            case 5:
            case 'end':
              return _context4.stop();
          }
        }
      }, _callee4, undefined);
    }));

    function refund(_x8, _x9) {
      return _ref12.apply(this, arguments);
    }

    return refund;
  }(),
  accept: function () {
    var _ref15 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(_ref13, _ref14) {
      var commit = _ref13.commit;
      var purchase = _ref14.purchase,
          transaction = _ref14.transaction;
      var item;
      return regeneratorRuntime.wrap(function _callee5$(_context5) {
        while (1) {
          switch (_context5.prev = _context5.next) {
            case 0:
              _context5.next = 2;
              return _skilikoSdk.Purchase.accept(purchase || transaction);

            case 2:
              item = _context5.sent;

              commit('updateItem', { item: item });
              return _context5.abrupt('return', item);

            case 5:
            case 'end':
              return _context5.stop();
          }
        }
      }, _callee5, undefined);
    }));

    function accept(_x10, _x11) {
      return _ref15.apply(this, arguments);
    }

    return accept;
  }()
};
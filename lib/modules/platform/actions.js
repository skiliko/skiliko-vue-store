'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _skilikoSdk = require('skiliko-sdk');

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.default = {
  fetch: function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(_ref) {
      var commit = _ref.commit,
          dispatch = _ref.dispatch,
          state = _ref.state;
      var platform;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              if (!state.current) {
                _context.next = 2;
                break;
              }

              return _context.abrupt('return', state.current);

            case 2:
              _context.next = 4;
              return _skilikoSdk.Platform.current();

            case 4:
              platform = _context.sent;

              commit('setCurrent', { platform: platform });
              if (platform.administrator) {
                commit('skiliko/experts/updateItem', { item: platform.administrator }, { root: true });
              }
              return _context.abrupt('return', platform);

            case 8:
            case 'end':
              return _context.stop();
          }
        }
      }, _callee, undefined);
    }));

    function fetch(_x) {
      return _ref2.apply(this, arguments);
    }

    return fetch;
  }()
};
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _skilikoSdk = require('skiliko-sdk');

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.default = {
  create: function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(_, _ref) {
      var email = _ref.email,
          title = _ref.title,
          message = _ref.message;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return _skilikoSdk.Support.message({
                email: email,
                title: title,
                message: message
              });

            case 2:
            case 'end':
              return _context.stop();
          }
        }
      }, _callee, undefined);
    }));

    function create(_x, _x2) {
      return _ref2.apply(this, arguments);
    }

    return create;
  }()
};
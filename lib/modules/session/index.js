'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _actions = require('./actions');

var _actions2 = _interopRequireDefault(_actions);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var state = {
  connected: false
};

var getters = {
  isConnected: function isConnected(state) {
    return state.connected;
  }
};

var mutations = {
  setConnected: function setConnected(state, _ref) {
    var connected = _ref.connected;
    return state.connected = connected;
  }
};

exports.default = {
  namespaced: true,
  state: state,
  getters: getters,
  mutations: mutations,
  actions: _actions2.default
};
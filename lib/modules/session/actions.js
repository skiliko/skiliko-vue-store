'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _skilikoSdk = require('skiliko-sdk');

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.default = {
  create: function () {
    var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(_ref, _ref2) {
      var commit = _ref.commit,
          dispatch = _ref.dispatch;
      var email = _ref2.email,
          password = _ref2.password;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              if (_skilikoSdk.Session.signedIn()) {
                _context.next = 3;
                break;
              }

              _context.next = 3;
              return _skilikoSdk.Session.login(email, password);

            case 3:
              _context.next = 5;
              return dispatch('skiliko/user/fetch', {}, { root: true });

            case 5:
              _context.next = 7;
              return dispatch('skiliko/chat/intialize', {}, { root: true });

            case 7:
              commit('setConnected', { connected: true });

            case 8:
            case 'end':
              return _context.stop();
          }
        }
      }, _callee, undefined);
    }));

    function create(_x, _x2) {
      return _ref3.apply(this, arguments);
    }

    return create;
  }(),
  destroy: function () {
    var _ref5 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(_ref4) {
      var commit = _ref4.commit;
      return regeneratorRuntime.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _context2.next = 2;
              return _skilikoSdk.Session.logout();

            case 2:
              commit('setConnected', { connected: false });
              commit('skiliko/user/setCurrent', { user: null }, { root: true });

            case 4:
            case 'end':
              return _context2.stop();
          }
        }
      }, _callee2, undefined);
    }));

    function destroy(_x3) {
      return _ref5.apply(this, arguments);
    }

    return destroy;
  }()
};
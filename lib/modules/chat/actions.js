'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _skilikoSdk = require('skiliko-sdk');

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var _chatInstance = null;

var handleCallUpdate = function handleCallUpdate(_ref, call) {
  var commit = _ref.commit,
      dispatch = _ref.dispatch,
      rootGetters = _ref.rootGetters;

  var transactions = rootGetters['skiliko/transactions/collection'];
  var transaction = Object.keys(transactions).map(function (x) {
    return transactions[x];
  }).find(function (x) {
    return parseInt(x.service.call_id, 10) === parseInt(call.callId, 10);
  });
  if (!transaction) {
    return;
  }
  var item = Object.assign({}, transaction, {
    service: Object.assign({}, transaction.service, call)
  });
  commit('skiliko/transactions/replaceItem', { item: item }, { root: true });
  return item.service.status === 'finished' ? Promise.all([dispatch('skiliko/user/fetch', { forceFetch: true }, { root: true }), dispatch('skiliko/transactions/get', { id: transaction.id }, { root: true })]) : Promise.resolve();
};

var handlePurchaseUpdate = function handlePurchaseUpdate(_ref2, purchase) {
  var dispatch = _ref2.dispatch;

  return dispatch('skiliko/transactions/get', { id: purchase.id }, { root: true });
};

exports.default = {
  intialize: function () {
    var _ref4 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(_ref3) {
      var commit = _ref3.commit,
          dispatch = _ref3.dispatch,
          rootGetters = _ref3.rootGetters;
      var currentUser;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              currentUser = rootGetters['skiliko/user/current'];

              if (currentUser) {
                _context.next = 3;
                break;
              }

              throw new Error('need to be connected');

            case 3:
              _context.next = 5;
              return _skilikoSdk.Chat.getChat(currentUser.chatable_id);

            case 5:
              _chatInstance = _context.sent;

              _chatInstance.onNewMessage(function (item) {
                return commit('skiliko/messages/updateItem', { item: item }, { root: true });
              }).onMessageReceived(function (item) {
                dispatch('skiliko/messages/markAsReceived', { id: item._id }, { root: true }).then(function (_) {
                  return commit('skiliko/messages/updateItem', { item: item }, { root: true });
                });
              }).onMessageRead(function (item) {
                return commit('skiliko/messages/updateItem', { item: item }, { root: true });
              }).onNewConversation(function (item) {
                return commit('skiliko/conversations/updateItem', { item: item }, { root: true });
              }).onCallUpdate(function (call) {
                return handleCallUpdate({ commit: commit, dispatch: dispatch, rootGetters: rootGetters }, call);
              }).onPurchaseUpdate(function (purchase) {
                return handlePurchaseUpdate({ dispatch: dispatch }, JSON.parse(purchase));
              });
              _context.next = 9;
              return dispatch('skiliko/messages/fetchPending', {}, { root: true });

            case 9:
              return _context.abrupt('return', _chatInstance);

            case 10:
            case 'end':
              return _context.stop();
          }
        }
      }, _callee, undefined);
    }));

    function intialize(_x) {
      return _ref4.apply(this, arguments);
    }

    return intialize;
  }(),
  get: function () {
    var _ref6 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(_ref5) {
      var dispatch = _ref5.dispatch,
          getters = _ref5.getters;
      var chat;
      return regeneratorRuntime.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _context2.next = 2;
              return _chatInstance ? Promise.resolve(_chatInstance) : dispatch('intialize');

            case 2:
              chat = _context2.sent;
              return _context2.abrupt('return', chat);

            case 4:
            case 'end':
              return _context2.stop();
          }
        }
      }, _callee2, undefined);
    }));

    function get(_x2) {
      return _ref6.apply(this, arguments);
    }

    return get;
  }()
};
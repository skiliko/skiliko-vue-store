'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _skilikoSdk = require('skiliko-sdk');

var _merge = require('../../utils/merge');

var _merge2 = _interopRequireDefault(_merge);

var _collection = require('../../extensions/collection');

var _collection2 = _interopRequireDefault(_collection);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

exports.default = (0, _merge2.default)({}, (0, _collection2.default)({
  fetchAll: _skilikoSdk.Offer.customOffers,
  get: _skilikoSdk.Offer.get
}), {
  namespaced: true,
  getters: {
    perCategory: function perCategory(state, getters, rootState, rootGetters) {
      var platform = rootGetters['skiliko/platform/current'];
      if (!platform) {
        return {};
      }
      var categories = (platform.settings.custom_offers || '').split(',').filter(function (x) {
        return x;
      });
      var pages = Object.keys(getters.collection).map(function (x) {
        return getters.collection[x];
      });
      return categories.reduce(function (acc, category) {
        return Object.assign({}, acc, _defineProperty({}, category.trim(), pages.filter(function (x) {
          return x.service_type === category;
        })));
      }, {});
    },
    legalPages: function legalPages(state, getters) {
      return getters.perCategory['legal'] || [];
    },
    terms: function terms(state, getters) {
      return getters.legalPages.filter(function (x) {
        return x.service_options.type === 'terms';
      })[0];
    },
    privacy: function privacy(state, getters) {
      return getters.legalPages.filter(function (x) {
        return x.service_options.type === 'privacy';
      })[0];
    },
    legal: function legal(state, getters) {
      return getters.legalPages.filter(function (x) {
        return x.service_options.type === 'legal';
      })[0];
    }
  }
});
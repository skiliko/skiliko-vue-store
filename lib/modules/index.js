'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _session = require('./session');

var _session2 = _interopRequireDefault(_session);

var _user = require('./user');

var _user2 = _interopRequireDefault(_user);

var _platform = require('./platform');

var _platform2 = _interopRequireDefault(_platform);

var _registration = require('./registration');

var _registration2 = _interopRequireDefault(_registration);

var _password = require('./password');

var _password2 = _interopRequireDefault(_password);

var _experts = require('./experts');

var _experts2 = _interopRequireDefault(_experts);

var _pages = require('./pages');

var _pages2 = _interopRequireDefault(_pages);

var _offers = require('./offers');

var _offers2 = _interopRequireDefault(_offers);

var _transactions = require('./transactions');

var _transactions2 = _interopRequireDefault(_transactions);

var _support = require('./support');

var _support2 = _interopRequireDefault(_support);

var _card = require('./card');

var _card2 = _interopRequireDefault(_card);

var _chat = require('./chat');

var _chat2 = _interopRequireDefault(_chat);

var _conversations = require('./conversations');

var _conversations2 = _interopRequireDefault(_conversations);

var _messages = require('./messages');

var _messages2 = _interopRequireDefault(_messages);

var _feedbacks = require('./feedbacks');

var _feedbacks2 = _interopRequireDefault(_feedbacks);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  session: _session2.default,
  user: _user2.default,
  platform: _platform2.default,
  registration: _registration2.default,
  password: _password2.default,
  experts: _experts2.default,
  pages: _pages2.default,
  offers: _offers2.default,
  transactions: _transactions2.default,
  support: _support2.default,
  card: _card2.default,
  chat: _chat2.default,
  conversations: _conversations2.default,
  messages: _messages2.default,
  feedbacks: _feedbacks2.default
};
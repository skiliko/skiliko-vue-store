import format from './formatHourMinuteMonth'

const dateIso = date => [
  date.getUTCFullYear(),
  format(date.getUTCMonth() + 1),
  format(date.getUTCDate())
].join('-')

const timeIso = date => [
  format(date.getUTCHours()),
  format(date.getUTCMinutes()),
  '00'
].join(':')

const iso = date => ({
  date: dateIso(date),
  hour: timeIso(date)
})

export default date => `${iso(date).date}T${iso(date).hour}.000Z`

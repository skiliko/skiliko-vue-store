const transform = data => Object.keys(data)
  .sort()
  .filter(e => data[e])
  .reduce((acc, item) => Object.assign({},
    acc,
    { [item]: data[item] }
  ), {})

export default data => Object.keys(transform(data))
  .map(item => [item, data[item]].join('='))
  .join('&') || 'default'

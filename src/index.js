import modules from './modules'
import mixins from './mixins'

export default store => store.registerModule('skiliko', {
  namespaced: true,
  modules
})

export {
  mixins
}

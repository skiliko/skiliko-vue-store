import { Offer } from 'skiliko-sdk'
import merge from '../../utils/merge'
import collection from '../../extensions/collection'

export default merge({},
  collection({
    fetchAll: Offer.customOffers,
    get: Offer.get
  }), {
    namespaced: true,
    getters: {
      perCategory (state, getters, rootState, rootGetters) {
        const platform = rootGetters['skiliko/platform/current']
        if (!platform) { return {} }
        const categories = (platform.settings.custom_offers || '').split(',').filter(x => x)
        const pages = Object.keys(getters.collection).map(x => getters.collection[x])
        return categories.reduce((acc, category) => Object.assign({},
          acc,
          { [category.trim()]: pages.filter(x => x.service_type === category) }
        ), {})
      },
      legalPages (state, getters) {
        return getters.perCategory['legal'] || []
      },
      terms (state, getters) {
        return getters.legalPages
          .filter(x => x.service_options.type === 'terms')[0]
      },
      privacy (state, getters) {
        return getters.legalPages
          .filter(x => x.service_options.type === 'privacy')[0]
      },
      legal (state, getters) {
        return getters.legalPages
          .filter(x => x.service_options.type === 'legal')[0]
      }
    }
  }
)

import { Offer } from 'skiliko-sdk'
import merge from '../../utils/merge'
import collection from '../../extensions/collection'

export default merge({},
  collection({
    get: Offer.get,
    fetchAll: Offer.publicOffers
  }), {
    namespaced: true
  }
)

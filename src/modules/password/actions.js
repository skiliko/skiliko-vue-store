import { Password } from 'skiliko-sdk'

export default {
  sendInstructions: async (_, { email }) => {
    await Password.sendInstructions(email)
  },
  reset: async (_, { token, password, passwordConfirmation }) => {
    await Password.reset(token, password, passwordConfirmation)
  }
}

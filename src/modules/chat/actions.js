import { Chat } from 'skiliko-sdk'

let _chatInstance = null

const handleCallUpdate = ({ commit, dispatch, rootGetters }, call) => {
  const transactions = rootGetters['skiliko/transactions/collection']
  const transaction = Object.keys(transactions)
    .map(x => transactions[x])
    .find(x => parseInt(x.service.call_id, 10) === parseInt(call.callId, 10))
  if (!transaction) { return }
  const item = Object.assign({}, transaction, {
    service: Object.assign({}, transaction.service, call)
  })
  commit('skiliko/transactions/replaceItem', { item }, { root: true })
  return item.service.status === 'finished'
    ? Promise.all([
      dispatch('skiliko/user/fetch', { forceFetch: true }, { root: true }),
      dispatch('skiliko/transactions/get', { id: transaction.id }, { root: true })
    ])
    : Promise.resolve()
}

const handlePurchaseUpdate = ({ dispatch }, purchase) => {
  return dispatch('skiliko/transactions/get', { id: purchase.id }, { root: true })
}

export default {
  intialize: async ({ commit, dispatch, rootGetters }) => {
    const currentUser = rootGetters['skiliko/user/current']
    if (!currentUser) { throw new Error('need to be connected') }
    _chatInstance = await Chat.getChat(currentUser.chatable_id)
    _chatInstance
      .onNewMessage(item => commit('skiliko/messages/updateItem', { item }, { root: true }))
      .onMessageReceived(item => {
        dispatch('skiliko/messages/markAsReceived', { id: item._id }, { root: true })
          .then(_ => commit('skiliko/messages/updateItem', { item }, { root: true }))
      })
      .onMessageRead(item => commit('skiliko/messages/updateItem', { item }, { root: true }))
      .onNewConversation(item => commit('skiliko/conversations/updateItem', { item }, { root: true }))
      .onCallUpdate(call => handleCallUpdate({ commit, dispatch, rootGetters }, call))
      .onPurchaseUpdate(purchase => handlePurchaseUpdate({ dispatch }, JSON.parse(purchase)))
    await dispatch('skiliko/messages/fetchPending', {}, { root: true })
    return _chatInstance
  },
  get: async ({ dispatch, getters }) => {
    const chat = await (_chatInstance
      ? Promise.resolve(_chatInstance)
      : dispatch('intialize'))
    return chat
  }
}

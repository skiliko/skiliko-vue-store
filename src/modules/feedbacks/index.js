import { Rating } from 'skiliko-sdk'
import merge from '../../utils/merge'
import collection from '../../extensions/collection'
import actions from './actions'

export default merge({},
  collection({
    fetchAll: payload => Rating.all(payload.page, payload.expertId)
  }), {
    namespaced: true,
    actions
  }
)

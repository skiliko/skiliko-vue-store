import { Rating } from 'skiliko-sdk'

export default {
  create: async ({ commit, dispatch }, { purchaseId, purchase, note, content }) => {
    const item = await Rating.create(
      purchaseId || purchase.id,
      note,
      content
    )
    commit('replaceItem', { item })
    return item
  }
}

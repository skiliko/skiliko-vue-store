import { Support } from 'skiliko-sdk'

export default {
  create: async (_, { email, title, message }) => {
    await Support.message({
      email,
      title,
      message
    })
  }
}

import actions from './actions'

const state = {
  connected: false
}

const getters = {
  isConnected: state => state.connected
}

const mutations = {
  setConnected: (state, { connected }) => (state.connected = connected)
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}

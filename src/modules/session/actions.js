import { Session } from 'skiliko-sdk'

export default {
  create: async ({ commit, dispatch }, { email, password }) => {
    if (!Session.signedIn()) {
      await Session.login(email, password)
    }
    await dispatch('skiliko/user/fetch', {}, { root: true })
    await dispatch('skiliko/chat/intialize', {}, { root: true })
    commit('setConnected', { connected: true })
  },
  destroy: async ({ commit }) => {
    await Session.logout()
    commit('setConnected', { connected: false })
    commit('skiliko/user/setCurrent', { user: null }, { root: true })
  }
}

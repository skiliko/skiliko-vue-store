import { Purchase } from 'skiliko-sdk'
import merge from '../../utils/merge'
import collection from '../../extensions/collection'
import actions from './actions'

export default merge({},
  collection({
    fetchAll: Purchase.all,
    get: Purchase.get
  }), {
    namespaced: true,
    actions
  }
)

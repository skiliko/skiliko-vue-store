import { Purchase } from 'skiliko-sdk'
import dateToIso from '../../utils/dateToIso'

const processData = (data = {}) => Object.assign({},
  data,
  {
    attachments: data.attachments ? JSON.stringify(data.attachments) : null,
    call_at: data.call_at ? dateToIso(new Date(data.call_at)) : null
  }
)

export default {
  create: async ({ commit, dispatch }, { offer, offerId, expert, expertId }) => {
    const platform = await dispatch('skiliko/platform/fetch', {}, { root: true })
    const item = await Purchase.create(
      offer || { id: offerId },
      platform.multi_experts ? (expert || { id: expertId }) : null
    )
    commit('updateItem', { item })
    return item
  },
  update: async ({ commit }, { purchase, transaction, data }) => {
    const item = await Purchase.save(
      purchase || transaction,
      processData(data)
    )
    commit('updateItem', { item })
    return item
  },
  pay: async ({ commit, dispatch }, { purchase, transaction, cardId, card }) => {
    const cardInformations = cardId || !card
      ? await Promise.resolve({ id: cardId })
      : await dispatch('skiliko/card/create', card, { root: true })
    const item = await Purchase.pay(Object.assign({},
      (purchase || transaction),
      { card_id: cardInformations.id }
    ))
    commit('updateItem', { item })
    return item
  },
  refund: async ({ commit }, { purchase, transaction }) => {
    const item = await Purchase.refund(purchase || transaction)
    commit('updateItem', { item })
    return item
  },
  accept: async ({ commit }, { purchase, transaction }) => {
    const item = await Purchase.accept(purchase || transaction)
    commit('updateItem', { item })
    return item
  }
}

import { Chat } from 'skiliko-sdk'

export default {
  fetchAll: async ({ commit, dispatch }, { conversationId, page = 1 } = { page: 1 }) => {
    const chat = await dispatch('skiliko/chat/get', {}, { root: true })
    const collection = await chat.messages(conversationId, page)
    commit('updateCollection', { collection, payload: { conversationId, page } })
    return collection
  },
  fetchPending: async ({ commit, dispatch }) => {
    const chat = await dispatch('skiliko/chat/get', {}, { root: true })
    const collection = await Chat.pendingMessages(chat.from)
    commit('updateCollection', { collection, payload: { scope: 'pendings' } })
    return collection
  },
  create: async ({ commit, dispatch }, { message, to, data }) => {
    const chat = await dispatch('skiliko/chat/get', {}, { root: true })
    const item = await chat.send(message, to, data)
    commit('replaceItem', { item })
    return item
  },
  markAllAsRead: async ({ commit, dispatch }, { ids }) => {
    const chat = await dispatch('skiliko/chat/get', {}, { root: true })
    await chat.markAsRead(ids)
  },
  markAsReceived: async ({ commit, dispatch, getters }, { id }) => {
    const chat = await dispatch('skiliko/chat/get', {}, { root: true })
    const message = getters.collection[id]
    if (!id) { return }
    if (message.from._id === chat.from) { return }
    if (message.receivedAt) { return }
    await chat.markAsRead(id)
  }
}

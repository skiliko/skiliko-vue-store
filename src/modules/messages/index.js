import merge from '../../utils/merge'
import collection from '../../extensions/collection'
import actions from './actions'

export default merge({},
  collection(), {
    namespaced: true,
    getters: {
      pending: (state, getters, rootState, rootGetters) => rootGetters['skiliko/user/current']
        ? Object.keys(getters.collection)
          .map(id => getters.collection[id])
          .filter(message => !!message.to)
          .filter(message => message.to.scope === 'user')
          .filter(message => message.to._id === rootGetters['skiliko/user/current'].chatable_id)
          .filter(message => !message.readAt)
        : []
    },
    actions
  }
)

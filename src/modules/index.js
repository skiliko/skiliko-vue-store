import session from './session'
import user from './user'
import platform from './platform'
import registration from './registration'
import password from './password'
import experts from './experts'
import pages from './pages'
import offers from './offers'
import transactions from './transactions'
import support from './support'
import card from './card'
import chat from './chat'
import conversations from './conversations'
import messages from './messages'
import feedbacks from './feedbacks'

export default {
  session,
  user,
  platform,
  registration,
  password,
  experts,
  pages,
  offers,
  transactions,
  support,
  card,
  chat,
  conversations,
  messages,
  feedbacks
}

import { User } from 'skiliko-sdk'

export default {
  fetch: async ({ commit }, { forceFetch }) => {
    const user = await User.current({ forceFetch })
    commit('setCurrent', { user })
  },
  save: async ({ commit }, data) => {
    const user = await User.save(data)
    commit('setCurrent', { user })
  }
}

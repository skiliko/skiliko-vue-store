import actions from './actions'

export default {
  namespaced: true,
  state: {
    current: null
  },
  getters: {
    current: state => state.current
  },
  mutations: {
    setCurrent: (state, { user }) => (state.current = user)
  },
  actions
}

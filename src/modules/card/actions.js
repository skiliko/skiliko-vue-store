import { Card } from 'skiliko-sdk'
import format from '../../utils/formatHourMinuteMonth'

export default {
  create: async ({ dispatch }, { number, month, year, cvx, firstName, lastName, birthday }) => {
    await dispatch('skiliko/user/save', { first_name: firstName, last_name: lastName, birthday }, { root: true })
    const card = await Card.create(number, format(month), year, cvx)
    await dispatch('skiliko/user/save', { card_id: card.id }, { root: true })
    return card
  }
}

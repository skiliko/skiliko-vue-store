import { Session } from 'skiliko-sdk'

export default {
  create: async ({ dispatch }, { email, password, passwordConfirmation, terms }) => {
    await Session.register({
      email,
      password,
      password_confirmation: passwordConfirmation,
      terms_accepted_at: terms ? new Date() : null
    })
    dispatch('skiliko/session/create', {}, { root: true })
  },
  destroy: async ({ dispatch }) => {
    await Session.unregister()
    dispatch('skiliko/session/destroy', {}, { root: true })
  }
}

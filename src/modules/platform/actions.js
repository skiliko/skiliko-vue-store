import { Platform } from 'skiliko-sdk'

export default {
  fetch: async ({ commit, dispatch, state }) => {
    if (state.current) { return state.current }
    const platform = await Platform.current()
    commit('setCurrent', { platform })
    if (platform.administrator) {
      commit('skiliko/experts/updateItem', { item: platform.administrator }, { root: true })
    }
    return platform
  }
}

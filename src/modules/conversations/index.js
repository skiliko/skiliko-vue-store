import merge from '../../utils/merge'
import collection from '../../extensions/collection'
import actions from './actions'

export default merge({},
  collection(), {
    namespaced: true,
    getters: {
      pending: (state, getters, rootState, rootGetters) => rootGetters['skiliko/messages/pending']
        .reduce((conversations, message) => Object.assign({},
          conversations,
          {
            [message.conversation]: Array.concat(
              (conversations[message.conversation] || []),
              [message]
            )
          }
        ), {})
    },
    actions
  }
)

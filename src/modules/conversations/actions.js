export default {
  fetchAll: async ({ commit, dispatch }, { page = 1 } = { page: 1 }) => {
    const chat = await dispatch('skiliko/chat/get', {}, { root: true })
    const collection = await chat.conversations(page)
    commit('updateCollection', { collection, payload: { page } })
    return collection
  },
  find: async ({ commit, dispatch }, { id }) => {
    const chat = await dispatch('skiliko/chat/get', {}, { root: true })
    const item = await chat.conversationWith(id)
    commit('updateItem', { item })
    return item
  }
}

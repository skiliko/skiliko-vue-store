import { Expert } from 'skiliko-sdk'

export default {
  fetchAll: async ({ commit, dispatch, rootGetters }, payload) => {
    const platform = rootGetters['skiliko/platform/current']
    if (!platform.multi_experts) { return }
    const collection = await Expert.all()
    commit('updateCollection', { collection, payload })
  }
}

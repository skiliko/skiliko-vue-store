import { Expert } from 'skiliko-sdk'
import merge from '../../utils/merge'
import collection from '../../extensions/collection'
import actions from './actions'

export default merge({},
  collection({
    get: Expert.get
  }), {
    namespaced: true,
    actions
  }
)

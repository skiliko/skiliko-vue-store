export default {
  computed: {
    platform () {
      return this.$store.getters['skiliko/platform/current']
    }
  }
}

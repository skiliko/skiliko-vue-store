import collectionKey from '../utils/collectionKey'

export default (collectionName, payload = x => ({})) => ({
  computed: {
    [`collections[${collectionName}]`] () {
      return this.$store.getters[`skiliko/${collectionName}/collection`]
    },
    [`pages[${collectionName}]`] () {
      return this.$store.getters[`skiliko/${collectionName}/pages`]
    },
    [`paginations[${collectionName}]`] () {
      return this.$store.getters[`skiliko/${collectionName}/paginations`]
    },
    [`${collectionName}Pagination`] () {
      const key = collectionKey(payload(this) || {})
      return (this[`paginations[${collectionName}]`][key] || {})
    },
    [collectionName] () {
      const key = collectionKey(payload(this) || {})
      return (this[`pages[${collectionName}]`][key] || [])
        .map(id => this[`collections[${collectionName}]`][id])
    }
  }
})

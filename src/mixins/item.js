import pluralize from '../utils/pluralize'

export default (resource, idAttribute = 'id') => ({
  computed: {
    [`${resource}Collection`] () {
      return this.$store.getters[`skiliko/${pluralize(resource)}/collection`]
    },
    [resource] () {
      const id = this[idAttribute] || this.$route.params[idAttribute]
      return this[`${resource}Collection`][id]
    }
  },
  mounted () {
    const idValue = this[idAttribute] || this.$route.params[idAttribute]
    if (!idValue) {
      throw new Error(`Cannot find an ID for the ${resource} with the attribute ${idAttribute}`)
    }
  }
})

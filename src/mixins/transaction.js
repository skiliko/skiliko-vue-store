export default (item = 'transaction') => ({
  computed: {
    transactionType () { return this[item].offer.service_type },
    isService () { return this.transactionType === 'service' },
    isMail () { return this.transactionType === 'mail' },
    isProduct () { return this.transactionType === 'product' },
    isThread () { return this.transactionType === 'thread' },
    isCall () { return this.transactionType === 'phone' },
    isPrepaidMinutes () { return this.transactionType === 'prepaid_minute' },

    isPayed () { return !!this[item].payed_at },
    isAccepted () { return !!this[item].accepted_at },
    isFinished () { return !!this[item].finished_at },
    isRefunded () { return !!this[item].refunded_at },
    needPayment () { return this[item].offer.price !== 0 },

    status () {
      if (this.isRefunded) { return 'refunded' }
      if (this.isFinished) { return 'finished' }
      if (this.isAccepted) { return 'inProgress' }
      if (this.isPayed) { return 'pending' }
      throw new Error(`Status is not valid for #${this[item].id}`)
    }
  }
})

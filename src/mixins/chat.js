export default withAttributeName => ({
  computed: {
    conversations () {
      return this.$store.getters['skiliko/conversations/collection']
    },
    conversationId () {
      const conversations = Object.keys(this.conversations)
        .filter(id => this.conversations[id].with._id === this[withAttributeName])
      if (conversations.length === 1) {
        return conversations[0]
      }
      return conversations.filter(id => id && id !== 'undefined')[0]
    },
    conversation () {
      return this.conversations[this.conversationId]
    },
    allMessages () {
      return this.$store.getters['skiliko/messages/collection']
    },
    messages () {
      return Object.keys(this.allMessages)
        .filter(x => typeof this.allMessages[x].conversation === 'string'
          ? this.allMessages[x].conversation === this.conversationId
          : this.allMessages[x].conversation._id === this.conversationId)
        .map(x => this.allMessages[x])
        .sort((a, b) => a.sentAt.localeCompare(b.sentAt))
    }
  },
  methods: {
    sendMessage ({ message, data = {} }) {
      return this.$store.dispatch('skiliko/messages/create', {
        message,
        data,
        to: this.conversation.with._id
      })
    },
    markAsRead ({ messages }) {
      const ids = this.messages
        .filter(message => !message.readAt)
        .filter(message => message.from._id === this.conversation.with._id)
        .map(m => m._id)
      return this.$store.dispatch('skiliko/messages/markAllAsRead', {
        ids
      })
    }
  }
})

export default {
  computed: {
    isConnected () {
      return this.$store.getters['skiliko/session/isConnected']
    },
    currentUser () {
      return this.$store.getters['skiliko/user/current']
    }
  },
  watch: {
    isConnected () { this.handleConnect() }
  },
  methods: {
    handleConnect () {
      if (this.isConnected && this.onLogin) { this.onLogin() }
      if (!this.isConnected && this.onLogout) { this.onLogout() }
    }
  },
  mounted () {
    this.handleConnect()
  }
}

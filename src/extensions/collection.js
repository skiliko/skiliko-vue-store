import collectionKey from '../utils/collectionKey'

const updateItem = (state, item) => replaceItem(state, Object.assign({},
  state.collectionById[(item.id || item._id)],
  item
))

const replaceItem = (state, item) => (state.collectionById = Object.assign({},
  state.collectionById,
  { [(item.id || item._id)]: item }
))

const updateCollection = (state, collection, payload = {}) => {
  state.collectionById = Object.assign({},
    state.collectionById,
    collection.reduce((acc, item) => Object.assign({},
      acc,
      { [(item.id || item._id)]: item }
    ), {})
  )
  state.pages = Object.assign({},
    state.pages,
    { [collectionKey(payload)]: collection.map(x => x.id || x._id) }
  )

  state.paginations = Object.assign({},
    state.paginations,
    { [collectionKey(payload)]: collection.pagination }
  )
}

const ACTIONS = {
  fetchAll: executor => async ({ commit }, payload) => {
    const collection = await executor(payload)
    commit('updateCollection', { collection, payload })
    return collection
  },
  get: executor => async ({ commit }, { id }) => {
    const item = await executor(id)
    commit('updateItem', { item })
    return item
  }
}

const generateActions = (actions = {}) => {
  if (Object.keys(actions).some(x => Object.keys(ACTIONS).indexOf(x) < 0)) {
    throw new Error(`Only actions ${Object.keys(ACTIONS).join(', ')} are available !`)
  }
  return Object.keys(actions)
    .reduce((acc, action) => Object.assign({},
      acc,
      { [action]: ACTIONS[action](actions[action]) }
    ), {})
}

export default (actions = {}) => ({
  state: {
    collectionById: {},
    pages: {},
    paginations: {}
  },
  getters: {
    collection: state => state.collectionById,
    pages: state => state.pages,
    paginations: state => state.paginations
  },
  mutations: {
    updateItem: (state, { item }) => updateItem(state, item),
    replaceItem: (state, { item }) => replaceItem(state, item),
    updateCollection: (state, { collection, payload }) => updateCollection(state, collection, payload)
  },
  actions: generateActions(actions)
})

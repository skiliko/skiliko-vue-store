export default {
  state: {
    isLoading: false
  },
  mutations: {
    changeLoading: (state, { isLoading }) => (state.isLoading = isLoading)
  },
  getters: {
    isLoading: state => state.isLoading
  }
}
